# G

## Content

```
./Gabriel Cristescu:
Gabriel Cristescu - Logica (Note de curs).pdf

./Gabriel Liiceanu:
Gabriel Liiceanu - Cearta cu filosofia.pdf
Gabriel Liiceanu - Dragul meu turnator.pdf

./Gabriel Marcel:
Gabriel Marcel - A fi si a avea.pdf
Gabriel Marcel - Omul problematic.pdf

./Gabriel Thoveron:
Gabriel Thoveron - Istoria mijloacelor de comunicare.pdf

./George Berkeley:
George Berkeley - Scrieri metafizice.pdf

./George Bondor:
George Bondor - Dansul mastilor.pdf

./George Edward Moore:
George Edward Moore - Principia ethica.pdf

./George Ene:
George Ene - Filosofia politica a lui John Stuart Mill.pdf

./George Orwell:
George Orwell - Ferma animalelor.pdf

./Georges Minois:
Georges Minois - Istoria infernurilor.pdf
Georges Minois - Istoria sinuciderii.pdf

./Georg Henrik von Wright:
Georg Henrik von Wright - Explicatie si intelegere.pdf
Georg Henrik von Wright - Norma si actiune.pdf

./Georg Wilhelm Friedrich Hegel:
Georg Wilhelm Friedrich Hegel - Fenomenologia spiritului.pdf
Georg Wilhelm Friedrich Hegel - Filosofia Spiritului.pdf
Georg Wilhelm Friedrich Hegel - Franz Wiedmannn.pdf
Georg Wilhelm Friedrich Hegel - Lectii despre Platon.pdf
Georg Wilhelm Friedrich Hegel - Prelegeri de istorie a filosofiei - vol. I.pdf
Georg Wilhelm Friedrich Hegel - Prelegeri de istorie a filosofiei - vol. II.pdf
Georg Wilhelm Friedrich Hegel - Principiile filosofiei dreptului.pdf
Georg Wilhelm Friedrich Hegel - Studii filosofice.pdf

./Gheorghe Enescu:
Gheorghe Enescu - Dictionar de logica.pdf
Gheorghe Enescu - Filozofie si logica.pdf
Gheorghe Enescu - Fundamentele logice ale gandirii.pdf
Gheorghe Enescu - Introducere in logica matematica.pdf
Gheorghe Enescu - Logica si adevar.pdf
Gheorghe Enescu - Logica simbolica.pdf
Gheorghe Enescu - Paradoxuri, sofisme, aporii.pdf
Gheorghe Enescu - Teoria sistemelor logice.pdf
Gheorghe Enescu - Tratat de logica.pdf

./Gheorghe Vladutescu:
Gheorghe Vladutescu - Etica lui Epicur.pdf
Gheorghe Vladutescu - O enciclopedie a filosofiei grecesti - vol I.pdf
Gheorghe Vladutescu - O istorie a ideilor filosofice.pdf
Gheorghe Vladutescu - Ontologie si metafizica la greci. Platon.pdf

./Gianfranco Pasquino:
Gianfranco Pasquino - Curs de stiinta politica.pdf

./Gianni Vattimo:
Gianni Vattimo - Gandirea slaba.pdf

./Gilles Deleuze:
Gilles Deleuze - Diferenta si repetitie.pdf
Gilles Deleuze - Foucault.pdf
Gilles Deleuze - Nietzsche.pdf
Gilles Deleuze - Nietzsche si filosofia.pdf

./Gilles Ferreol:
Gilles Ferreol - Dictionar de sociologie.pdf

./Gilles Lipovetsky:
Gilles Lipovetsky - Amurgul datoriei.pdf

./Giordano Bruno:
Giordano Bruno - Opere italiene I.pdf
Giordano Bruno - Opere italiene II.pdf
Giordano Bruno - Opere italiene III.pdf
Giordano Bruno - Opere italiene IV.pdf
Giordano Bruno - Opere italiene V.pdf
Giordano Bruno - Opere italiene VI.pdf
Giordano Bruno - Opere italiene VII.pdf

./Giovanni Papini:
Giovanni Papini - Amurgul filosofilor.pdf

./Giovanni Pico della Mirandola:
Giovanni Pico della Mirandola - Rationamente sau 900 de teze (Despre demnitatea omului).pdf

./Giovanni Reale:
Giovanni Reale - Istoria filosofiei antice, vol. 1.pdf
Giovanni Reale - Istoria filosofiei antice, vol. 2.pdf
Giovanni Reale - Istoria filosofiei antice, vol. 3.pdf
Giovanni Reale - Istoria filosofiei antice, vol. 4.pdf

./Gottfried Wilhelm Leibniz:
Gottfried Wilhelm Leibniz - Eseuri de teodicee.pdf
Gottfried Wilhelm Leibniz - Primae Veritates.pdf
Gottfried Wilhelm Leibniz - Scrieri filosofice.pdf

./Gottlob Frege:
Gottlob Frege - Despre sens si semnificatie.pdf
Gottlob Frege - Fundamentele aritmeticii.pdf
Gottlob Frege - Scrieri logico-filosofice I.pdf

./Graham Priest:
Graham Priest - Dincolo de limitele gandirii.pdf

./Gregory Vlastos:
Gregory Vlastos - Socrate (Ironie si filozofie morala).pdf

./Grigore Moisil:
Grigore Moisil - Elemente de logica matematica si de teoria multimilor.pdf
Grigore Moisil - Lectii despre logica rationamentului nuantat.pdf

./Grigore Tausan:
Grigore Tausan - Filosofia lui Plotin.pdf

./Gyorgy Lukacs:
Gyorgy Lukacs - Ontologia existentei sociale.pdf
```

